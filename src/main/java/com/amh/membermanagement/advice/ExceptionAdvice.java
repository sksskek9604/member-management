package com.amh.membermanagement.advice;

import com.amh.membermanagement.enums.common.ResultCode;
import com.amh.membermanagement.exception.common.CMissingDataException;
import com.amh.membermanagement.exception.daily_check.*;
import com.amh.membermanagement.exception.login.CWhenLoginPasswordException;
import com.amh.membermanagement.exception.login.CWhenLoginResignMemberException;
import com.amh.membermanagement.exception.login.CWhenLoginUserNameException;
import com.amh.membermanagement.exception.vacation.CImpossibleApprovalVacation;
import com.amh.membermanagement.exception.vacation.CImpossibleEqualApprovalStatus;
import com.amh.membermanagement.exception.vacation.CImpossibleRequestVacation;
import com.amh.membermanagement.model.common.CommonResult;
import com.amh.membermanagement.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) { //기본 비상구, 기본 실패하였습니다.
        return ResponseService.getFailResult(ResultCode.FAILED); // 실패하였습니다.
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) //사용자가 잘못이면 400번으로 보내줌
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA); // 데이터를 찾을 수 없습니다.
    }

    // 인자가 다르기 때문에 exception 개수만큼 만들어줘야함.
    @ExceptionHandler(CWhenLoginUserNameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWhenLoginUserNameException e) {
        return ResponseService.getFailResult(ResultCode.NOT_FOUND_USERNAME); // 아이디를 찾을 수 없을 때
    }

    @ExceptionHandler(CWhenLoginPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWhenLoginPasswordException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PASSWORD); // 비밀번호 틀렸을 때
    }

    @ExceptionHandler(CWhenLoginResignMemberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request,  CWhenLoginResignMemberException e) {
        return ResponseService.getFailResult(ResultCode.RESIGN_MEMBER); // 탈퇴한 회원인 경우일 때
    }

    @ExceptionHandler(CChangeAfterWorkOutException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request,  CChangeAfterWorkOutException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_CHANGE_AFTER_WORK_OUT); // 퇴근 후 다시 상태를 변화시킬 수 없을 때
    }

    @ExceptionHandler(CChangeAgainWorkStartException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request,  CChangeAgainWorkStartException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_CHANGE_AGAIN_WORK_START); // 또 출근으로 바꿀 수 없을 때
    }

    @ExceptionHandler(CChangeEqualWorkStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request,  CChangeEqualWorkStateException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_CHANGE_EQUAL_WORK_STATE); // 같은 근태 상태로는 다시 변경할 수 없습니다.
    }

    @ExceptionHandler(CMissingDataWorkStartException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request,  CMissingDataWorkStartException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA_WORK_START); // 출근 기록이 없습니다.
    }

    @ExceptionHandler(CChangeBeforeShortOutException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CChangeBeforeShortOutException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_CHANGE_BEFORE_SHORT_OUT); // 외출 전에는 외출복귀를 할 수 없습니다.
    }

    @ExceptionHandler(CChangeAfterShortOutException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CChangeAfterShortOutException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_CHANGE_AFTER_SHORT_OUT); // 외출 복귀 후 다시 외출을 할 수 없습니다.
    }

    @ExceptionHandler(CChangeAfterTimeEarlyLeaveException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CChangeAfterTimeEarlyLeaveException e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_CHANGE_AFTER_TIME_EARLY_LEAVE); // 조퇴 후 다시 퇴근을 할 수 없습니다.
    }

    @ExceptionHandler(CImpossibleRequestVacation.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CImpossibleRequestVacation e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_REQUEST_VACATION); // 직원의 연차 수량이 부족합니다.
    }

    @ExceptionHandler(CImpossibleApprovalVacation.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CImpossibleApprovalVacation e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_APPROVAL_VACATION); // 신청한 연차가 반려되었습니다.
    }

    @ExceptionHandler(CImpossibleEqualApprovalStatus.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CImpossibleEqualApprovalStatus e) {
        return ResponseService.getFailResult(ResultCode.IMPOSSIBLE_EQUAL_VACATION_APPROVAL_STATUS); // 승인상태를 동일하게 바꿀 수 없습니다.
    }

}
