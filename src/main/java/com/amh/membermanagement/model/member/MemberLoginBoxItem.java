package com.amh.membermanagement.model.member;

import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberLoginBoxItem {

    @ApiModelProperty(value = "직원 시퀀스")
    private Long memberId;

    @ApiModelProperty(value = "직원 이름")
    private String memberName;

    @ApiModelProperty(value = "직원 아이디 + 회사 이메일")
    private String fullUsername;

    private MemberLoginBoxItem(MemberLoginBoxItemBuilder builder) {
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.fullUsername = builder.fullUsername;
    }
    public static class MemberLoginBoxItemBuilder implements CommonModelBuilder<MemberLoginBoxItem> {
        private final Long memberId;
        private final String memberName;
        private final String fullUsername;
        public MemberLoginBoxItemBuilder(Member member) {
            this.memberId = member.getId();
            this.memberName = "이름 : " + member.getMemberName();
            this.fullUsername = member.getUsername() + "@js.co.kr";
        }

        @Override
        public MemberLoginBoxItem build() {
            return new MemberLoginBoxItem(this);
        }
    }


}
