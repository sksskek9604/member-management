package com.amh.membermanagement.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberLoginRequest {


    @ApiModelProperty(value = "아이디", required = true)
    @NotNull
    @Length(min = 5, max = 15)
    private String username;

    @ApiModelProperty(value = "비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String password;
}
