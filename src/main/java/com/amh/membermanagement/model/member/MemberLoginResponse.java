package com.amh.membermanagement.model.member;

import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberLoginResponse {
    // 앱에서 갖고 있을 불변하는 정보
    @ApiModelProperty(value = "회원시퀀스")
    private Long memberId;

    @ApiModelProperty(value = "아이디")
    private String username;

    @ApiModelProperty(value = "입사일")
    private LocalDate dateJoin;

    public MemberLoginResponse(MemberLoginResponseBuilder builder) {
        this.memberId = builder.memberId;
        this.username = builder.username;
        this.dateJoin = builder.dateJoin;
    }

    public static class MemberLoginResponseBuilder implements CommonModelBuilder<MemberLoginResponse> {

        private final Long memberId;
        private final String username;
        private final LocalDate dateJoin;

        public MemberLoginResponseBuilder(Member member) {
            this.memberId = member.getId();
            this.username = member.getUsername();
            this.dateJoin = member.getDateJoin();
        }

        @Override
        public MemberLoginResponse build() {
            return new MemberLoginResponse(this);
        }
    }
}
