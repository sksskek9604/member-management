package com.amh.membermanagement.model.member.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberResignUpdate {

    @ApiModelProperty(value = "퇴사날짜")
    @NotNull
    private LocalDate dateResign;
}
