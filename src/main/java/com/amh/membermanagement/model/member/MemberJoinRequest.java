package com.amh.membermanagement.model.member;

import com.amh.membermanagement.enums.member.Gender;
import com.amh.membermanagement.enums.member.Position;
import com.amh.membermanagement.enums.member.Team;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {

    @ApiModelProperty(value = "직원 아이디", required = true)
    @NotNull
    @Length(min = 8, max = 15)
    private String username;

    @ApiModelProperty(value = "비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 15)
    private String password;

    @ApiModelProperty(value = "직원 이름", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;

    @ApiModelProperty(value = "부서", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Team team;

    @ApiModelProperty(value = "직급", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Position position;

    @ApiModelProperty(value = "성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(value = "직원 연락처", required = true)
    @NotNull
    @Length(min = 12, max = 13)
    private String memberPhone;

    @ApiModelProperty(value = "직원 주소", required = true)
    @NotNull
    @Length(min = 9, max = 40)
    private String memberAddress;

    @ApiModelProperty(value = "입사일", required = true)
    @NotNull
    private LocalDate dateJoin;

}


