package com.amh.membermanagement.model.member;

import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberMyPageItem {

    @ApiModelProperty(value = "직원 시퀀스")
    private Long id;

    @ApiModelProperty(value = "관리자 권한")
    private Boolean isAdmin;

    @ApiModelProperty(value = "아이디")
    private String username;

    @ApiModelProperty(value = "비밀번호")
    private String password;

    @ApiModelProperty(value = "팀+이름+직급")
    private String fullName;
    @ApiModelProperty(value = "성별")
    private String gender;

    @ApiModelProperty(value = "연락처")
    private String memberPhone;

    @ApiModelProperty(value = "주소")
    private String memberAddress;

    @ApiModelProperty(value = "입사년도")
    private LocalDate dateJoin;

    @ApiModelProperty(value = "퇴사년도")
    private LocalDate dateResign;

    @ApiModelProperty(value = "퇴사 여부")
    private Boolean isWorking;

    @ApiModelProperty(value = "생성 시간")
    private LocalDateTime dateCreate;

    @ApiModelProperty(value = "수정 시간")
    private LocalDateTime dateUpdate;
    private MemberMyPageItem(MemberItemBuilder builder) {
        this.id = builder.id;
        this.isAdmin = builder.isAdmin;
        this.username = builder.username;
        this.password = builder.password;
        this.fullName = builder.fullName;
        this.gender = builder.gender;
        this.memberPhone = builder.memberPhone;
        this.memberAddress = builder.memberAddress;
        this.dateJoin = builder.dateJoin;
        this.dateResign = builder.dateResign;
        this.isWorking = builder.isWorking;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class MemberItemBuilder implements CommonModelBuilder<MemberMyPageItem> {
        private final Long id;
        private final Boolean isAdmin;
        private final String username;
        private final String password;
        private final String fullName;
        private final String gender;
        private final String memberPhone;
        private final String memberAddress;
        private final LocalDate dateJoin;
        private final LocalDate dateResign;
        private final Boolean isWorking;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.isAdmin = member.getIsAdmin();
            this.username = member.getUsername();
            this.password = member.getPassword();
            this.fullName = member.getTeam().getName() + " " + member.getMemberName() + " " + member.getPosition().getName();
            this.gender = member.getGender().getName();
            this.memberPhone = member.getMemberPhone();
            this.memberAddress = member.getMemberAddress();
            this.dateJoin = member.getDateJoin();
            this.dateResign = member.getDateResign();
            this.isWorking = member.getIsWorking();
            this.dateCreate = member.getDateCreate();
            this.dateUpdate = member.getDateUpdate();
        }

        @Override
        public MemberMyPageItem build() {
            return new MemberMyPageItem(this);
        }
    }
}
