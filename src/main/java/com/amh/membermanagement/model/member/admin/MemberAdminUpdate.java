package com.amh.membermanagement.model.member.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberAdminUpdate {

    @ApiModelProperty(value = "관리자 권한")
    @NotNull
    private Boolean isAdmin;
}
