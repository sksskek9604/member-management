package com.amh.membermanagement.model.member.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberPasswordInit {

    @ApiModelProperty(value = "비밀번호")
    @Length(min = 8, max = 15)
    private String password ;


}
