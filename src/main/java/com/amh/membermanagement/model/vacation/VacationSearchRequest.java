package com.amh.membermanagement.model.vacation;

import com.amh.membermanagement.enums.member.Team;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class VacationSearchRequest {

    // 필수값 2개 선택값 하나죠?
    // select 박스를 실제로 보게되면

    @ApiModelProperty(value = "직원 시퀀스")
    private Long memberId;

    @ApiModelProperty(value = "팀(부서)")
    private Team team;

    @NotNull
    @ApiModelProperty(value = "조회할 날짜 시작")
    private LocalDate dateStart;

    @NotNull
    @ApiModelProperty(value = "조회할 날짜 끝")
    private LocalDate dateEnd;
}
