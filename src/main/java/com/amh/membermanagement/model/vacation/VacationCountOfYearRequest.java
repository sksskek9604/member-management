package com.amh.membermanagement.model.vacation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class VacationCountOfYearRequest {

    @ApiModelProperty(value = "직원 시퀀스")
    private Long member;

    @ApiModelProperty(value = "입사년도")
    private LocalDate dateJoin;
}
