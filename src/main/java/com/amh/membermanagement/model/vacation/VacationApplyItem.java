package com.amh.membermanagement.model.vacation;

import com.amh.membermanagement.entity.VacationTotalUsage;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationApplyItem {
    @ApiModelProperty(value = "종합 휴가 내역 시퀀스")
    private Long vacationTotalUsageId;
    @ApiModelProperty(value = "휴가 신청 정보(간략)")
    private String vacationApplyRequestSimpleInfo;
    @ApiModelProperty(value = "휴가 신청 정보(날짜, 요일, 신청 연차 수량)")
    private String vacationApplyValueInfo;
    private VacationApplyItem(VacationApplyItemBuilder builder) {
        this.vacationTotalUsageId = builder.vacationTotalUsageId;
        this.vacationApplyRequestSimpleInfo = builder.vacationApplyRequestSimpleInfo;
        this.vacationApplyValueInfo = builder.vacationApplyValueInfo;
    }
    public static class VacationApplyItemBuilder implements CommonModelBuilder<VacationApplyItem> {
        private final Long vacationTotalUsageId;
        private final String vacationApplyRequestSimpleInfo;
        private final String vacationApplyValueInfo;

        public VacationApplyItemBuilder(VacationTotalUsage vacationTotalUsage) {
            this.vacationTotalUsageId = vacationTotalUsage.getId();
            this.vacationApplyRequestSimpleInfo = "(" + vacationTotalUsage.getMember().getTeam().getName() + ") " + vacationTotalUsage.getMember().getMemberName() + " " + vacationTotalUsage.getVacationType().getName() + " 신청의 건";
            this.vacationApplyValueInfo = vacationTotalUsage.getDateApproving().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)) + " " + vacationTotalUsage.getVacationType() + " (" + String.format("%.1f", vacationTotalUsage.getIncreaseOrDecreaseValue()) + "일)";
        }
        @Override
        public VacationApplyItem build() {
            return new VacationApplyItem(this);
        }
    }

}
