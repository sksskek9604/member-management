package com.amh.membermanagement.model.vacation;

import com.amh.membermanagement.enums.vacation.ApprovalStatus;
import com.amh.membermanagement.enums.vacation.VacationType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class VacationAdminRequest { // 특별휴가 주는 용도

    @ApiModelProperty(value = "휴가 종류")
    @NotNull
    @Enumerated(EnumType.STRING)
    private VacationType vacationType;
    @NotNull
    @Length(max = 20)
    @ApiModelProperty(value = "신청 사유")
    private String vacationReason;

    @ApiModelProperty(value = "증감 개수")
    private Float increaseOrDecreaseValue;

}
