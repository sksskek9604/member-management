package com.amh.membermanagement.model.vacation;

import com.amh.membermanagement.enums.vacation.ApprovalStatus;
import com.amh.membermanagement.enums.vacation.VacationType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class VacationMemberRequest {

    @ApiModelProperty(value = "휴가 종류")
    @NotNull
    @Enumerated(EnumType.STRING)
    private VacationType vacationType;

    @NotNull
    @ApiModelProperty(value = "휴가 시작일")
    private LocalDate vacationStart;

    @NotNull
    @ApiModelProperty(value = "휴가 종료일")
    private LocalDate vacationEnd;

    @NotNull
    @Length(max = 20)
    @ApiModelProperty(value = "신청 사유")
    private String vacationReason;


    @ApiModelProperty(value = "증감 개수")
    private Float increaseOrDecreaseValue;

}



