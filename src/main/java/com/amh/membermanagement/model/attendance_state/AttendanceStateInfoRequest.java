package com.amh.membermanagement.model.attendance_state;

import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.enums.vacation.ApprovalStatus;
import com.amh.membermanagement.enums.attendance_state.ShiftWorkType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AttendanceStateInfoRequest {

    @NotNull
    @ApiModelProperty(name = "직원 시퀀스")
    private Member memberId;

    @NotNull
    @Length(max = 40)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(name = "근태 종류")
    private ShiftWorkType shiftWorkType;

    @NotNull
    @Length(max = 30)
    @ApiModelProperty(name = "근태 변경 사유")
    private String shiftWorkReason;

    @NotNull
    @Length(max = 30)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(name = "승인 상태")
    private ApprovalStatus approvalStatus;

    @NotNull
    @ApiModelProperty(name = "근무 시간")
    private Short workTime;
}
