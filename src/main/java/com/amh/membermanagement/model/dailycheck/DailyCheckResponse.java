package com.amh.membermanagement.model.dailycheck;

import com.amh.membermanagement.entity.DailyCheck;
import com.amh.membermanagement.enums.daily_check.DailyCheckState;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DailyCheckResponse {
    @ApiModelProperty(value = "한국어 - 출근/조퇴/퇴근")
    private String dailyCheckStateName;
    @ApiModelProperty(value = "ENUM-key값")
    private String dailyCheckState;

    private DailyCheckResponse(DailyCheckResponseBuilder builder) {
        this.dailyCheckStateName = builder.dailyCheckStateName;
        this.dailyCheckState = builder.dailyCheckState;
    }

    private DailyCheckResponse(DailyCheckResponseUnknownBuilder builder) {
        this.dailyCheckStateName = builder.dailyCheckStateName;
        this.dailyCheckState = builder.dailyCheckState;
    }


    public static class DailyCheckResponseBuilder implements CommonModelBuilder<DailyCheckResponse> {
        private final String dailyCheckStateName;
        private final String dailyCheckState;

        public DailyCheckResponseBuilder(DailyCheck dailyCheck) {
            this.dailyCheckStateName = dailyCheck.getDailyCheckState().getName();
            this.dailyCheckState = dailyCheck.getDailyCheckState().toString();
        }
        @Override
        public DailyCheckResponse build() {
            return new DailyCheckResponse(this);
        }
    }

    public static class DailyCheckResponseUnknownBuilder implements CommonModelBuilder<DailyCheckResponse> {
        private final String dailyCheckStateName;
        private final String dailyCheckState;

        public DailyCheckResponseUnknownBuilder() {
            this.dailyCheckStateName = DailyCheckState.UNKNOWN.getName();
            this.dailyCheckState = DailyCheckState.UNKNOWN.toString();
        }
        @Override
        public DailyCheckResponse build() {
            return new DailyCheckResponse(this);
        }
    }
}
