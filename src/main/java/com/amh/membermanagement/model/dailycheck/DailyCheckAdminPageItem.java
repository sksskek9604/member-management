package com.amh.membermanagement.model.dailycheck;

import com.amh.membermanagement.entity.DailyCheck;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DailyCheckAdminPageItem {

    @ApiModelProperty(value = "출/퇴/외 기록 시퀀스")
    private Long id;

    @ApiModelProperty(value = "직원 시퀀스")
    private Long memberId;

    @ApiModelProperty(value = "근태 상태")
    private String dailyCheckState;

    @ApiModelProperty(value = "기준일")
    private LocalDate dateBase;

    @ApiModelProperty(value = "출근 시간")
    private LocalTime dateWorkStart;

    @ApiModelProperty(value = "외출 시간")
    private LocalTime dateShortOuting;

    @ApiModelProperty(value = "외출 복귀 시간")
    private LocalTime dateWorkComeBack;

    @ApiModelProperty(value = "퇴근 시간")
    private LocalTime dateWorkEnd;

    @ApiModelProperty(value = "수정 시간")
    private LocalDateTime dateUpdate;

    private DailyCheckAdminPageItem(DailyCheckAdminPageItemBuilder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.dailyCheckState = builder.dailyCheckState;
        this.dateBase = builder.dateBase;
        this.dateWorkStart = builder.dateWorkStart;
        this.dateShortOuting = builder.dateShortOuting;
        this.dateWorkComeBack = builder.dateWorkComeBack;
        this.dateWorkEnd = builder.dateWorkEnd;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class DailyCheckAdminPageItemBuilder implements CommonModelBuilder<DailyCheckAdminPageItem> {
        private final Long id;
        private final Long memberId;
        private final String dailyCheckState;
        private final LocalDate dateBase;
        private final LocalTime dateWorkStart;
        private final LocalTime dateShortOuting;
        private final LocalTime dateWorkComeBack;
        private final LocalTime dateWorkEnd;
        private final LocalDateTime dateUpdate;
        public DailyCheckAdminPageItemBuilder(DailyCheck dailyCheck) {
            this.id = dailyCheck.getId();
            this.memberId = dailyCheck.getMember().getId();
            this.dailyCheckState = dailyCheck.getDailyCheckState().getName();
            this.dateBase = dailyCheck.getDateBase();
            this.dateWorkStart = dailyCheck.getDateWorkStart();
            this.dateShortOuting = dailyCheck.getDateShortOuting();
            this.dateWorkComeBack = dailyCheck.getDateWorkComeBack();
            this.dateWorkEnd = dailyCheck.getDateWorkEnd();
            this.dateUpdate = dailyCheck.getDateUpdate();
        }
        @Override
        public DailyCheckAdminPageItem build() {
            return new DailyCheckAdminPageItem(this);
        }
    }
}
