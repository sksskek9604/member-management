package com.amh.membermanagement.model.dailycheck;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DailyCheckSearchRequest {

    @ApiModelProperty(value = "직원 시퀀스")
    private Long member;

    @ApiModelProperty(value = "기준일")
    private LocalDate dateBase;



}
