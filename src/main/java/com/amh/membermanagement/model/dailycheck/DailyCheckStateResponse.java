package com.amh.membermanagement.model.dailycheck;

import com.amh.membermanagement.enums.daily_check.DailyCheckState;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class DailyCheckStateResponse {

    // 현재 근태 상태 가져오는 용도의 모델
    @ApiModelProperty(value = "근태 상태")
    @Enumerated(EnumType.STRING)
    private DailyCheckState dailyCheckState;

    private DailyCheckStateResponse(DailyCheckStateResponseBuilder builder) {
        this.dailyCheckState = builder.dailyCheckState;
    }
    public static class DailyCheckStateResponseBuilder implements CommonModelBuilder<DailyCheckStateResponse> {
        private final DailyCheckState dailyCheckState;
        public DailyCheckStateResponseBuilder(DailyCheckState dailyCheckState) {
            this.dailyCheckState = dailyCheckState;
        }
        @Override
        public DailyCheckStateResponse build() {
            return new DailyCheckStateResponse(this);
        }
    }
}
