package com.amh.membermanagement.model.company_news;

import com.amh.membermanagement.entity.CompanyNews;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CompanyNewsItem {

    @ApiModelProperty(value = "뉴스 시퀀스", required = true)
    private Long id;

    @ApiModelProperty(value = "제목", required = true)
    private String headline;

    @ApiModelProperty(value = "내용", required = true)
    private String contents;

    private CompanyNewsItem(CompanyNewsItemBuilder builder) {
        this.id = builder.id;
        this.headline = builder.headline;
        this.contents = builder.contents;
    }

    public static class CompanyNewsItemBuilder implements CommonModelBuilder<CompanyNewsItem> {
        private final Long id;
        private final String headline;
        private final String contents;

        public CompanyNewsItemBuilder(CompanyNews companyNews) {
            this.id = companyNews.getId();
            this.headline = companyNews.getHeadline();
            this.contents = companyNews.getContents();

        }
        @Override
        public CompanyNewsItem build() {
            return new CompanyNewsItem(this);
        }
    }
}
