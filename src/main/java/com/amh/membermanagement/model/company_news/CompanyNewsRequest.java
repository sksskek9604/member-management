package com.amh.membermanagement.model.company_news;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CompanyNewsRequest {

    @NotNull
    @Length(max = 20)
    @ApiModelProperty(value = "제목", required = true)
    private String headline;

    @NotNull
    @Length(max = 150)
    @ApiModelProperty(value = "내용", required = true)
    private String contents;

}
