package com.amh.membermanagement.controller;

import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.entity.VacationTotalUsage;
import com.amh.membermanagement.enums.vacation.ApprovalStatus;
import com.amh.membermanagement.model.common.CommonResult;
import com.amh.membermanagement.model.common.ListResult;
import com.amh.membermanagement.model.vacation.VacationAdminRequest;
import com.amh.membermanagement.model.vacation.VacationApplyItem;
import com.amh.membermanagement.model.vacation.VacationMemberRequest;
import com.amh.membermanagement.service.MemberService;
import com.amh.membermanagement.service.VacationService;
import com.amh.membermanagement.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "휴가 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/vacation")
public class VacationController {
    private final MemberService memberService;
    private final VacationService vacationService;

    @ApiOperation(value = "직원 - 연차신청")
    @PostMapping("/post-memberVacation/{memberId}")
    public CommonResult setMemberRequestVacation(@PathVariable long memberId, @RequestBody VacationMemberRequest request) {
        Member member = memberService.getMemberData(memberId);
        vacationService.setMemberRequestVacation(member, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "관리자 - 특별휴가 연차 기입")
    @PostMapping("/post-vacationPlusAdmin/{memberId}")
    public CommonResult settVacationCount(@PathVariable long memberId, @RequestBody VacationAdminRequest request) {
        Member member = memberService.getMemberData(memberId);
        vacationService.setVacationPlusAdmin(member, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "전체 직원 휴가 신청 리스트 가져오기(기준: 승인, 대기, 반려)")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "approvalStatus", value = "승인, 대기, 반려", required = true)
    )
    @GetMapping("/approval-status")
    public ListResult<VacationApplyItem> getVacationApplyList(@RequestParam ApprovalStatus approvalStatus) {
        return ResponseService.getListResult(vacationService.getVacationApplyList(approvalStatus), true);
    }

    @ApiOperation(value = "직원별 휴가 신청 리스트 가져오기(기준: 승인, 대기, 반려)")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "approvalStatus", value = "승인, 대기, 반려", required = true)
    )
    @GetMapping("/approval-status/{memberId}")
    public ListResult<VacationApplyItem> getVacationApplyMemberList(@PathVariable long memberId, @RequestParam(value = "approvalStatus", required = false) ApprovalStatus approvalStatus) {
        return ResponseService.getListResult(vacationService.getVacationApplyMemberList(memberService.getMemberData(memberId), approvalStatus), true);
    }

    @ApiOperation(value = "직원별 휴가 신청 전체 리스트 가져오기")
    @GetMapping("/all/{memberId}")
    public ListResult<VacationApplyItem> getVacationApplyMemberListAll(@PathVariable long memberId) {
        return ResponseService.getListResult(vacationService.getVacationApplyMemberListAll(memberService.getMemberData(memberId)), true);
    }

    @ApiOperation(value = "관리자 - 휴가 신청 처리 및 연차 수량 변경")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "vacationTotalUsageId", value = "휴가종합내역 시퀀스", required = true)
    )
    @PutMapping("/put-approval/{vacationTotalUsageId}")
    public CommonResult putVacationApproval(@PathVariable long vacationTotalUsageId, @RequestParam ApprovalStatus approvalStatus) {
        VacationTotalUsage vacationTotalUsage = vacationService.getVacationApprovingData(vacationTotalUsageId);
        vacationService.putVacationApprovalAdmin(vacationTotalUsage, approvalStatus);
        return ResponseService.getSuccessResult();
    }


}
