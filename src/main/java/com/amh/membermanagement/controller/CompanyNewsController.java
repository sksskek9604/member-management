package com.amh.membermanagement.controller;

import com.amh.membermanagement.model.company_news.CompanyNewsItem;
import com.amh.membermanagement.model.company_news.CompanyNewsRequest;
import com.amh.membermanagement.model.common.CommonResult;
import com.amh.membermanagement.model.common.ListResult;
import com.amh.membermanagement.model.common.SingleResult;
import com.amh.membermanagement.service.CompanyNewsService;
import com.amh.membermanagement.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회사 뉴스 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/company-news")
public class CompanyNewsController {
    private final CompanyNewsService companyNewsService;

    @ApiOperation(value = "회사 뉴스 등록")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid CompanyNewsRequest request) {
        companyNewsService.setCompanyNews(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회사 뉴스 id별 리스트")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyNewsId", value = "회사 목차 번호(시퀀스)", required = true)
    })
    @GetMapping("/company-news/list/{companyNewsId}")
    public SingleResult<CompanyNewsItem> getCompanyNews(@PathVariable long companyNewsId) {
        return ResponseService.getSingleResult(companyNewsService.getCompanyNews(companyNewsId));
    }

    @ApiOperation(value = "회사 뉴스 전체 리스트")
    @GetMapping("/all")
    public ListResult<CompanyNewsItem> getCompanyNewsAll() {
        return ResponseService.getListResult(companyNewsService.getCompanyNewsAll(), true);
    }

}
