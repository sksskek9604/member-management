package com.amh.membermanagement.controller.member;
import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.model.common.CommonResult;
import com.amh.membermanagement.model.common.SingleResult;
import com.amh.membermanagement.model.member.*;
import com.amh.membermanagement.service.MemberService;
import com.amh.membermanagement.service.VacationService;
import com.amh.membermanagement.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "일반 직원용 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("v1/login-member")
public class MemberLoginController {

    private final MemberService memberService;
    private final VacationService vacationService;

    @ApiOperation(value = "직원 정보 등록 + 연차 테이블 초기 생성")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest request) {
        Member member = memberService.setMember(request);
        vacationService.setVacation(member.getId(), member.getDateJoin()); // 연차 갯수를 등록한다.
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 로그인")
    @PostMapping("/login") // post인 이유는 결국 body를 받아야해서 put은 수정의 개념이다보니 < post
    public SingleResult<MemberLoginResponse> setMemberLogin(@RequestBody @Valid MemberLoginRequest loginRequest) {
        return ResponseService.getSingleResult(memberService.setMemberDoLogin(loginRequest)); // 계정 1개로 공유하기 때문에, admin도 어플 사용해야하므로 수정함
    }

    @ApiOperation(value = "우측 상단 로그인 박스")
    @ApiImplicitParams({@ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)})
    @GetMapping("/login-box/{memberId}")
    public SingleResult<MemberLoginBoxItem> getMemberLoginBox(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberService.getMemberLoginBox(memberId));
    }

    @ApiOperation(value = "직원 마이페이지")
    @ApiImplicitParams({@ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)})
    @GetMapping("/my-page/{memberId}")
    public SingleResult<MemberMyPageItem> getMemberMyPage(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberService.getMemberMyPage(memberId));
    }

}
