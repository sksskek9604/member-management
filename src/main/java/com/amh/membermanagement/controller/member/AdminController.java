package com.amh.membermanagement.controller.member;

import com.amh.membermanagement.model.common.CommonResult;
import com.amh.membermanagement.model.common.ListResult;
import com.amh.membermanagement.model.common.SingleResult;
import com.amh.membermanagement.model.member.*;
import com.amh.membermanagement.model.member.admin.MemberAdminUpdate;
import com.amh.membermanagement.model.member.admin.MemberPasswordInit;
import com.amh.membermanagement.model.member.admin.MemberResignUpdate;
import com.amh.membermanagement.service.MemberService;
import com.amh.membermanagement.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "관리자용 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login-admin")
public class AdminController {

    private final MemberService memberService;

    @ApiOperation(value = "관리자 로그인")
    @PostMapping("/login")
    public SingleResult<MemberLoginResponse> setAdminLogin(@RequestBody @Valid MemberLoginRequest loginRequest) {
        return ResponseService.getSingleResult(memberService.setMemberAdminLogin(true, loginRequest));
    }

    @ApiOperation(value = "우측 상단 로그인 박스")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "직원 시퀀스", required = true)
    })
    @GetMapping("/login-box/{id}")
    public SingleResult<MemberLoginBoxItem> getMemberLoginBox(@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMemberLoginBox(id));
    }


    @ApiOperation(value = "직원 정보 리스트")
    @GetMapping("/all")
    public ListResult<MemberMyPageItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers(), true);
    }

    @ApiOperation(value = "직원 정보 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "직원 시퀀스", required = true)
    })
    @PutMapping("/member-info/{id}")
    public CommonResult putMemberInfo(@PathVariable long id, @RequestBody @Valid MemberJoinRequest memberJoinRequest) {
        memberService.putMemberInfo(id, memberJoinRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 퇴사 처리 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "직원 시퀀스", required = true)
    })
    @PutMapping("/member-resign/{id}")
    public CommonResult putMemberResign(@PathVariable long id, @RequestBody @Valid MemberResignUpdate memberResignUpdate) {
        memberService.putMemberResign(id, memberResignUpdate);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 관리자 권한 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "직원 시퀀스", required = true)
    })
    @PutMapping("/member-is-admin/{id}")
    public CommonResult putMemberAdmin(@PathVariable long id, @RequestBody @Valid MemberAdminUpdate isAdminUpdate) {
        memberService.putMemberAdmin(id, isAdminUpdate);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 password 초기화") // Todo 직원 번호 뒷자리 or 0000 분류는 추가 예정 -> 12345678 8글자 이상으로 함.
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "직원 시퀀스", required = true)
    })
    @PutMapping("/member-password-init/{id}")
    public CommonResult putMemberAdmin(@PathVariable long id, @RequestBody @Valid MemberPasswordInit memberPasswordInit) {
        memberService.putMemberPasswordInit(id, memberPasswordInit);
        return ResponseService.getSuccessResult();
    }


}
