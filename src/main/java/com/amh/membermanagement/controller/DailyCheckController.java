package com.amh.membermanagement.controller;

import com.amh.membermanagement.enums.daily_check.DailyCheckState;
import com.amh.membermanagement.model.common.ListResult;
import com.amh.membermanagement.model.common.SingleResult;
import com.amh.membermanagement.model.dailycheck.DailyCheckMemberMyPageItem;
import com.amh.membermanagement.model.dailycheck.DailyCheckResponse;
import com.amh.membermanagement.service.DailyCheckService;
import com.amh.membermanagement.service.MemberService;
import com.amh.membermanagement.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "출퇴근 관리API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/daily-check")
public class DailyCheckController {
    private final DailyCheckService dailyCheckService;
    private final MemberService memberService;

    @ApiOperation(value = "출근/외출/퇴근 상태 변경")
    @PutMapping("/status/{dailyCheckState}/member-id/{memberId}")
    public SingleResult<DailyCheckResponse> doStateChange(
            @PathVariable long memberId,
            @PathVariable DailyCheckState dailyCheckState) {
        return ResponseService.getSingleResult(dailyCheckService.doDailyCheckChange(memberService.getMemberData(memberId), dailyCheckState));
    }

    @ApiOperation(value = "오늘 근태 상태 가져오기")
    @GetMapping("/status/member-id/{memberId}")
    public SingleResult<DailyCheckResponse> getState(@PathVariable long memberId) {
        return ResponseService.getSingleResult(dailyCheckService.getCurrentState(memberService.getMemberData(memberId)));
    }

    @ApiOperation(value = "직원별 출퇴근 전체 기록 가져오기")
    @GetMapping("/status/all/member-id/{memberId}")
    public ListResult<DailyCheckMemberMyPageItem> getDailyCheckMemberPageData(@PathVariable long memberId) {
        return ResponseService.getListResult(dailyCheckService.getDailyCheckMemberPage(memberService.getMemberData(memberId)), true);
    }

}
