package com.amh.membermanagement.enums.daily_check;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DailyCheckState {
    WORK_START("출근"),
    SHORT_OUTING("외출"),
    COME_BACK("외출 복귀"),
    TIME_EARLY_LEAVE("조퇴"),
    WORK_END("퇴근"),
    UNKNOWN("상태 없음");

    private final String name;
}
