package com.amh.membermanagement.enums.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {

    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")

    , NOT_FOUND_USERNAME(-20000, "아이디가 존재하지 않습니다.")
    , WRONG_PASSWORD(-20001, "잘못된 비밀번호입니다.")
    , RESIGN_MEMBER(-20002, "퇴사자입니다. 관리자에게 문의하세요.")

    , IMPOSSIBLE_CHANGE_EQUAL_WORK_STATE(-30000, "같은 근태 상태로는 다시 변경할 수 없습니다.")
    , IMPOSSIBLE_CHANGE_AGAIN_WORK_START(-30001, "근태 상태를 다시 출근으로 변경할 수 없습니다.")
    , IMPOSSIBLE_CHANGE_AFTER_WORK_OUT(-30002, "퇴근 후에는 상태를 다시 변경할 수 없습니다.")
    , MISSING_DATA_WORK_START(-30003, "출근 기록이 없습니다.")
    , IMPOSSIBLE_CHANGE_BEFORE_SHORT_OUT(-30003, "외출 전에는 외출복귀를 할 수 없습니다.")
    , IMPOSSIBLE_CHANGE_AFTER_SHORT_OUT(-30004, "외출 복귀 후 다시 외출을 할 수 없습니다.")
    , IMPOSSIBLE_CHANGE_AFTER_TIME_EARLY_LEAVE(-30005, "조퇴 후 다시 퇴근을 할 수 없습니다.")

    , IMPOSSIBLE_REQUEST_VACATION(-40000, "직원의 연차 수량이 부족합니다.")

    , IMPOSSIBLE_APPROVAL_VACATION(-50000, "신청한 연차가 반려되었습니다.")
    , IMPOSSIBLE_EQUAL_VACATION_APPROVAL_STATUS(-50001, "승인상태를 동일하게 바꿀 수 없습니다.")
    ;

    private final Integer code;
    private final String msg;
}
