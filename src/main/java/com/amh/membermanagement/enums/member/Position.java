package com.amh.membermanagement.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Position {
    CEO("대표")
    , GENERAL_MANAGER("부장")
    , MANAGER("과장")
    , SENIOR_STAFF("주임")
    , STAFF("직원")
    ;

    private final String name;
}
