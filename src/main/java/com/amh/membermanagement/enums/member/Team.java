package com.amh.membermanagement.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Team {
    MANAGEMENT_DEPT("경영관리부")
    , PERSONNEL_SECTION("인사관리팀")
    , DEVELOPMENT_DEPT("개발팀")
    , MARKETING_DEPT("마케팅팀")
    , NETWORK_DEPT("네트워크팀")
    , QUALITY_CONTROL_DEPT("품질관리팀")
    , CEO("대표")
    ;

    private final String name;
}
