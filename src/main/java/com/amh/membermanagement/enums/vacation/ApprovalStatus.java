package com.amh.membermanagement.enums.vacation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApprovalStatus {
    APPROVAL("승인"),
    REFER("반려"),
    STAND_BY("대기중");

    private final String name;
}
