package com.amh.membermanagement.enums.vacation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VacationType {

    ANNUAL_LEAVE("연차"),
    MONTHLY_LEAVE("월차"),
    HALF_DAY_LEAVE("반차"),
    SICK_LEAVE("병가"),
    ANNUAL_LEAVE_PLUS("연차 추가"); // 무급 상황 등 고려해야하는 사항이 추가될 예정

    private final String name;
}
