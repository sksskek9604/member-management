package com.amh.membermanagement.enums.attendance_state;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ShiftWorkType {

    OUT_ON_BUSINESS("외근"),
    EXTRA_WORK("연장근무"),
    TELECOMMUTING("재택근무"),
    BUSINESS_TRIP("출장");

    private final String name;
}
