package com.amh.membermanagement.configure;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // 전체라는 뜻
                .allowedOrigins("*")
                .allowedMethods("*")
                .maxAge(3000);
        //주소를 적으면 거기만 풀어준다는 것임 *는 전체 풀어준다고 한 것.

    }

    /*
    로컬 서버 설정. 이 설정을 하지 않으면
    이미지가 업로드 되어도 서버를 다시 재시작해서
    업로드된 이미지를 서버시작과 동시에 알려주어야만
    이미지 접근이 됨.

    application.yml 파일에 추가된 설정 같이 확인.
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String connectPath = "/static/**";
        String resourcePath = "file:///C:/workspace/java/member-management/src/main/resources/static/";
        registry.addResourceHandler(connectPath)
                .addResourceLocations(resourcePath);
    }

}
