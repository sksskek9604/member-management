package com.amh.membermanagement.repository;

import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.entity.VacationCount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VacationCountRepository extends JpaRepository<VacationCount, Long> {
 Optional<VacationCount> findByMemberIdAndCountAvailable(long memberId, float countAvailable);

}
