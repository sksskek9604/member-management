package com.amh.membermanagement.repository;

import com.amh.membermanagement.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsernameAndIsAdmin(String username, boolean isAdmin);

    Optional<Member> findByUsername(String username);
}
