package com.amh.membermanagement.repository;

import com.amh.membermanagement.entity.VacationTotalUsage;
import com.amh.membermanagement.enums.member.Team;
import com.amh.membermanagement.enums.vacation.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface VacationTotalUsageRepository extends JpaRepository<VacationTotalUsage, Long> {

    Page<VacationTotalUsage> findAllByMember_IdAndApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(Long member_Id, ApprovalStatus approvalStatus, LocalDateTime dateStart, LocalDateTime dateEnd, Pageable pageable);
    List<VacationTotalUsage> findAllByApprovalStatusOrderByDateApproving(ApprovalStatus approvalStatus);
    List<VacationTotalUsage> findAllByMember_IdAndApprovalStatusOrderByDateApproving(Long memberId, ApprovalStatus approvalStatus);

    List<VacationTotalUsage> findAllByMember_IdOrderByDateApproving(Long memberId);


    // 리스트를 페이지별로 쪼갠다고 했어요. 내가 요청한게 몇페이지인지 요런것들을 지금 넣지 않은 상태예요.
// 이럴때는 마지막에 Pageable 하면 3개가 뜨는데 date.domain이 들어간걸로 골라줍니다.
// 몇번째 페이지 주세요. 라는게 Pageable이에요. // 지금은 기준일이 없어서, dateCreate가 들어가 있지만 기준일 만들어서 넣어주면 됩니다.

    // 내일 수정할 곳
    Page<VacationTotalUsage> findAllByMember_TeamAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(Team team, LocalDateTime dateStart, LocalDateTime dateEnd, Pageable pageable);
    Optional<VacationTotalUsage> countByMember_IdOrMember_TeamAndVacationStart(Long member_id,Team team, LocalDate vacationStart);
    Optional<VacationTotalUsage> countByMember_IdAndVacationStart(Long member_id,LocalDate vacationStart);

    Page<VacationTotalUsage> findAllByApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(ApprovalStatus approvalStatus, LocalDateTime dateStart, LocalDateTime dateEnd, Pageable pageable);

}
