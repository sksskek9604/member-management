package com.amh.membermanagement.repository;

import com.amh.membermanagement.entity.CompanyNews;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyNewsRepository extends JpaRepository<CompanyNews, Long> {
}
