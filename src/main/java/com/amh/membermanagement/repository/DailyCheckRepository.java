package com.amh.membermanagement.repository;

import com.amh.membermanagement.entity.DailyCheck;
import com.amh.membermanagement.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface DailyCheckRepository extends JpaRepository<DailyCheck, Long> {
    Optional<DailyCheck> findByDateBaseAndMember_Id(LocalDate dateBase, long memberId);
    List<DailyCheck> findByIdAndMember_Id(long id,long memberId);
    List<DailyCheck> findByMember_Id(long memberId);

    long countByMemberAndDateBaseGreaterThanEqualAndDateBaseLessThanEqual(Member member, LocalDate dateStart, LocalDate dateEnd);
}
