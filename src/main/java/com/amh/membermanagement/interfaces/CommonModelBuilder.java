package com.amh.membermanagement.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
