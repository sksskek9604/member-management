package com.amh.membermanagement.service;

import com.amh.membermanagement.entity.DailyCheck;
import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.enums.daily_check.DailyCheckState;
import com.amh.membermanagement.exception.daily_check.*;
import com.amh.membermanagement.model.common.ListResult;
import com.amh.membermanagement.model.dailycheck.DailyCheckMemberMyPageItem;
import com.amh.membermanagement.model.dailycheck.DailyCheckResponse;
import com.amh.membermanagement.repository.DailyCheckRepository;
import com.amh.membermanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DailyCheckService {
    private final DailyCheckRepository dailyCheckRepository;

    /**
     * 현재 출근 상태 가져오기
     * @param member 직원 시퀀스
     * @return 출근 상태의 ENUM(+상태없음)과 출근상태의 한글명
     */
    public DailyCheckResponse getCurrentState(Member member) {
        Optional<DailyCheck> dailyCheck = dailyCheckRepository.findByDateBaseAndMember_Id(LocalDate.now(), member.getId());

        if (dailyCheck.isEmpty()) return new DailyCheckResponse.DailyCheckResponseUnknownBuilder().build();
        else return new DailyCheckResponse.DailyCheckResponseBuilder(dailyCheck.get()).build();
    }

    /**
     * 출근, 출근 상태 변경하기
     * @param member 직원 시퀀스
     * @param dailyCheckState 출근 상태 ENUM
     * @return 출근/출근상태 변경
     */
    public DailyCheckResponse doDailyCheckChange(Member member, DailyCheckState dailyCheckState) {
        Optional<DailyCheck> dailyCheck = dailyCheckRepository.findByDateBaseAndMember_Id(LocalDate.now(), member.getId());

        DailyCheck dailyCheckResult;
        if (dailyCheck.isEmpty()) dailyCheckResult = setDailyCheck(member);
        else dailyCheckResult = putDailyCheck(dailyCheck.get(), dailyCheckState);
        return new DailyCheckResponse.DailyCheckResponseBuilder(dailyCheckResult).build();
    }

    /**
     * 출근하기
     * @param member 직원 시퀀스
     * @return 출근 상태로 변경
     */
    private DailyCheck setDailyCheck(Member member) {
        DailyCheck data = new DailyCheck.DailyCheckBuilder(member).build();
        return dailyCheckRepository.save(data);
    }

    /**
     * 외출/조퇴/퇴근 하기
     * @param dailyCheck 출근 Entity
     * @param dailyCheckState 출근상태 ENUM
     * @return 근태 상태 변경
     */
    private DailyCheck putDailyCheck(DailyCheck dailyCheck, DailyCheckState dailyCheckState) {
        if (dailyCheckState.equals(DailyCheckState.COME_BACK)
                && !dailyCheck.getDailyCheckState().equals(DailyCheckState.SHORT_OUTING)) throw new CChangeBeforeShortOutException();
        // 1. 외출상태가 아니면 외출복귀를 사용하지 못한다. = 외출 상태에서만 외출복귀가 가능하다.
        if (dailyCheck.getDailyCheckState().equals(DailyCheckState.COME_BACK)
                && dailyCheckState.equals(DailyCheckState.SHORT_OUTING))
            throw new CChangeAfterShortOutException();
        // 2. 외출 복귀 후 다시 외출을 하지 못함.
        if (dailyCheckState.equals(dailyCheckState.WORK_START)) throw new CChangeAgainWorkStartException();
        if (dailyCheck.getDailyCheckState().equals(dailyCheckState)) throw new CChangeEqualWorkStateException();
        if (dailyCheck.getDailyCheckState().equals(dailyCheckState.WORK_END) || dailyCheck.getDailyCheckState().equals(dailyCheckState.TIME_EARLY_LEAVE)) throw new CChangeAfterWorkOutException();
        if (dailyCheck.getDailyCheckState().equals(dailyCheckState.TIME_EARLY_LEAVE)) throw new CChangeAfterTimeEarlyLeaveException();

        dailyCheck.putStateDailyCheck(dailyCheckState);
        return dailyCheckRepository.save(dailyCheck);
    }

    /*
     private로 한 다음, 리턴값을 entity로 받으면서 위 public에서 변환하고 있음.
     이렇게 하면 프론트에서 response로 하나로 받아서 set,put둘다 사용 가능
     */

    /**
     * 출퇴근 기록 조회하기
     * @param member 직원 시퀀스
     * @return 출퇴근 기록 조회
     */
    public ListResult<DailyCheckMemberMyPageItem> getDailyCheckMemberPage(Member member) {
        List<DailyCheck> dailyChecks = dailyCheckRepository.findByMember_Id(member.getId());

        List<DailyCheckMemberMyPageItem> result = new LinkedList<>();

        dailyChecks.forEach(dailyCheck -> {
            DailyCheckMemberMyPageItem addItem = new DailyCheckMemberMyPageItem.DailyCheckMemberMyPageItemBuilder(dailyCheck).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

}
