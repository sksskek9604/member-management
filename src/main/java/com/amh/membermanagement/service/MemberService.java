package com.amh.membermanagement.service;

import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.exception.common.CMissingDataException;
import com.amh.membermanagement.exception.login.CWhenLoginPasswordException;
import com.amh.membermanagement.exception.login.CWhenLoginResignMemberException;
import com.amh.membermanagement.exception.login.CWhenLoginUserNameException;
import com.amh.membermanagement.model.common.ListResult;
import com.amh.membermanagement.model.member.*;
import com.amh.membermanagement.model.member.admin.MemberAdminUpdate;
import com.amh.membermanagement.model.member.admin.MemberPasswordInit;
import com.amh.membermanagement.model.member.admin.MemberResignUpdate;
import com.amh.membermanagement.repository.MemberRepository;
import com.amh.membermanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;

    public Member getMemberData(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return member;
    }

    /**
     * 직원 app 로그인 하기(관리자 계정 포함)
     *
     * @param loginRequest 로그인 정보
     * @return 직원 app 로그인
     */
    public MemberLoginResponse setMemberDoLogin(MemberLoginRequest loginRequest) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CWhenLoginUserNameException::new); // throw 추가 "아이디가 존재하지 않습니다."
        // 회원 정보에 저장된 비밀번호와 일치하는가?
        if (!member.getPassword().equals(loginRequest.getPassword()))
            throw new CWhenLoginPasswordException(); // throw 추가 "잘못된 비밀번호입니다."

        // 퇴사한 상태라면?
        if (!member.getIsWorking()) throw new CWhenLoginResignMemberException();
        return new MemberLoginResponse.MemberLoginResponseBuilder(member).build(); // throw 추가 "퇴사자 입니다. 관리자에게 문의하세요."
    }

    /**
     * 관리자 전용 app 로그인
     *
     * @param isAdmin      관리자 여부
     * @param loginRequest 로그인 정보
     * @return 관리자 app 로그인
     */
    public MemberLoginResponse setMemberAdminLogin(boolean isAdmin, MemberLoginRequest loginRequest) {
        Member member = memberRepository.findByUsernameAndIsAdmin(loginRequest.getUsername(), isAdmin).orElseThrow(CWhenLoginUserNameException::new); // throw 추가 "아이디가 존재하지 않습니다."
        // 회원 정보에 저장된 비밀번호와 일치하는가?
        if (!member.getPassword().equals(loginRequest.getPassword()))
            throw new CWhenLoginPasswordException(); // throw 추가 "잘못된 비밀번호입니다."

        // 퇴사한 상태라면?
        if (!member.getIsWorking()) throw new CWhenLoginResignMemberException();
        return new MemberLoginResponse.MemberLoginResponseBuilder(member).build(); // throw 추가 "퇴사자 입니다. 관리자에게 문의하세요."
    }

    /**
     * 직원(입사자) 등록하기
     *
     * @param request 등록 폼
     * @return 직원 등록
     */
    public Member setMember(MemberJoinRequest request) {
        Member member = new Member.MemberBuilder(request).build();
        memberRepository.save(member);
        return member;
    }

    /**
     * app 우측 상단 로그인 정보
     *
     * @param id 직원 시퀀스
     * @return 직원이름 + 직원 이메일 @회사 주소
     */
    // 우측 상단 로그인 정보
    public MemberLoginBoxItem getMemberLoginBox(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MemberLoginBoxItem.MemberLoginBoxItemBuilder(member).build();
    }

    /**
     * 직원 마이페이지 정보 불러오기
     *
     * @param id 직원 시퀀스
     * @return 직원 마이페이지 정보
     */
    public MemberMyPageItem getMemberMyPage(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MemberMyPageItem.MemberItemBuilder(member).build();
    }

    /**
     * 전체 직원 정보 가져오기
     * @return 직원 내역 전체정보
     */
    public ListResult<MemberMyPageItem> getMembers() {
        List<Member> members = memberRepository.findAll();
        List<MemberMyPageItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberMyPageItem addItem = new MemberMyPageItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }


    /**
     * 직원 정보 수정하기
      * @param id 직원 시퀀스
     * @param request 수정 폼
     */
    public void putMemberInfo(long id, MemberJoinRequest request) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putMemberInfo(request);
        memberRepository.save(member);
    }

    /**
     * 직원 퇴사 처리하기
     * @param id 직원 시퀀스
     * @param resignUpdate 퇴사 폼
     */
    public void putMemberResign(long id, MemberResignUpdate resignUpdate) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putMemberResign(resignUpdate);
        memberRepository.save(member);
    }

    /**
     * 직원 관리자 권한 부여하기
     * @param id 직원 시퀀스
     * @param isAdminUpdate 관리자 권한 폼
     */
    public void putMemberAdmin(long id, MemberAdminUpdate isAdminUpdate) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putMemberAdmin(isAdminUpdate);
        memberRepository.save(member);
    }

    /**
     * 직원 비밀번호 초기화
     * @param id 직원 시퀀스
     * @param passwordInit 비밀번호 초기화 폼
     */
    public void putMemberPasswordInit(long id, MemberPasswordInit passwordInit) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putMemberPassword(passwordInit);
        memberRepository.save(member);
    }

}
