package com.amh.membermanagement.service;

import com.amh.membermanagement.entity.Member;
import com.amh.membermanagement.entity.VacationCount;
import com.amh.membermanagement.entity.VacationTotalUsage;
import com.amh.membermanagement.enums.vacation.ApprovalStatus;
import com.amh.membermanagement.enums.vacation.VacationType;
import com.amh.membermanagement.exception.common.CMissingDataException;
import com.amh.membermanagement.exception.vacation.CImpossibleApprovalVacation;
import com.amh.membermanagement.exception.vacation.CImpossibleEqualApprovalStatus;
import com.amh.membermanagement.exception.vacation.CImpossibleRequestVacation;
import com.amh.membermanagement.model.common.ListResult;
import com.amh.membermanagement.model.member.admin.MemberAdminUpdate;
import com.amh.membermanagement.model.vacation.*;
import com.amh.membermanagement.repository.VacationCountRepository;
import com.amh.membermanagement.repository.VacationTotalUsageRepository;
import com.amh.membermanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static com.amh.membermanagement.enums.vacation.ApprovalStatus.REFER;

@Service
@RequiredArgsConstructor
public class VacationService {

    // 연차 신청내역과 연차 잔여수는 연관되어있는 관계이기 때문에 vacation서비스 안에서 묶음.
    private final VacationCountRepository vacationCountRepository;
    private final VacationTotalUsageRepository vacationTotalUsageRepository;


    /**
     * 직원등록시 연차 초기값 생성
     *
     * @param memberId 직원 시퀀스
     * @param dateJoin 입사일(기준일)
     */
    public void setVacation(long memberId, LocalDate dateJoin) {
        VacationCount vacationCount = new VacationCount.VacationCountBuilder(memberId, dateJoin).build();
        vacationCountRepository.save(vacationCount);
    }

    /**
     * 직원의 연차 신청 등록
     *
     * @param member  직원 시퀀스
     * @param request 신청 폼
     */
    //Todo 이후 회사 규칙에 따라, 연차개수가 0이어도 '특별휴가 신청' 가능하도록 할지 안할지 정한다.
    // (현재는 0개면 1년차 미만이라 가정, 신입의 특별휴가는 최초는 관리자만 넣어줄 수 있다.)
    public void setMemberRequestVacation(Member member, VacationMemberRequest request) {
        VacationCount vacationCount = vacationCountRepository.findById(member.getId()).orElseThrow(CMissingDataException::new);
        if (vacationCount.getCountAvailable() <= 0)
            throw new CImpossibleRequestVacation(); // 사용가능 연차개수가 0개면 '연차 수량이 부족합니다'.

        VacationTotalUsage vacationTotalUsage = new VacationTotalUsage.VacationTotalUsageBuilder(member, request).build();

        // 사용 가능 연차 개수보다 신청 연차가 높으면 throw
        if (vacationTotalUsage.getIncreaseOrDecreaseValue() > vacationCount.getCountAvailable())
            throw new CImpossibleRequestVacation();
        vacationTotalUsageRepository.save(vacationTotalUsage);
    }

    /**
     * 관리자 특별휴가 신청(기본적으로 승인상태)
     *
     * @param member  직원 시퀀스
     * @param request 특별휴가 신청 폼
     */
    public void setVacationPlusAdmin(Member member, VacationAdminRequest request) {
        VacationCount vacationCount = vacationCountRepository.findById(member.getId()).orElseThrow(CMissingDataException::new);
        switch (request.getVacationType()) {
            case ANNUAL_LEAVE_PLUS:
                vacationCount.plusCountAvailable(request.getIncreaseOrDecreaseValue());
                vacationCount.plusCountTotal(request.getIncreaseOrDecreaseValue());
        }
        vacationCountRepository.save(vacationCount);

        VacationTotalUsage vacationTotalUsage = new VacationTotalUsage.VacationAdminTotalUsageBuilder(member, request).build();
        vacationTotalUsageRepository.save(vacationTotalUsage);
    }

    /**
     * 휴가 신청 직원 전체 리스트 가져오기 (대기중, 승인, 반려)
     *
     * @param approvalStatus 대기,승인,반려 enum
     * @return 가공된 휴가신청 리스트
     */
    public ListResult<VacationApplyItem> getVacationApplyList(ApprovalStatus approvalStatus) {
        List<VacationTotalUsage> vacationTotalUsages = vacationTotalUsageRepository.findAllByApprovalStatusOrderByDateApproving(approvalStatus);

        List<VacationApplyItem> result = new LinkedList<>();

        vacationTotalUsages.forEach(vacationTotalUsage -> {
            VacationApplyItem vacationApplyItem = new VacationApplyItem.VacationApplyItemBuilder(vacationTotalUsage).build();
            result.add(vacationApplyItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 휴가 신청 직원별로 리스트 가져오기 (대기중, 승인, 반려)
     *
     * @param approvalStatus 대기,승인,반려 enum
     * @return 가공된 휴가신청 리스트
     */
    public ListResult<VacationApplyItem> getVacationApplyMemberList(Member member, ApprovalStatus approvalStatus) {
        List<VacationTotalUsage> vacationTotalUsages = vacationTotalUsageRepository.findAllByMember_IdAndApprovalStatusOrderByDateApproving(member.getId(), approvalStatus);

        List<VacationApplyItem> result = new LinkedList<>();

        vacationTotalUsages.forEach(vacationTotalUsage -> {
            VacationApplyItem vacationApplyMemberItem = new VacationApplyItem.VacationApplyItemBuilder(vacationTotalUsage).build();
            result.add(vacationApplyMemberItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 휴가 신청 직원 내역 전체 리스트 가져오기
     *
     * @param member 직원 시퀀스
     * @return 직원별 휴가 전체 내역
     */
    public ListResult<VacationApplyItem> getVacationApplyMemberListAll(Member member) {
        List<VacationTotalUsage> vacationTotalUsages = vacationTotalUsageRepository.findAllByMember_IdOrderByDateApproving(member.getId());

        List<VacationApplyItem> result = new LinkedList<>();

        vacationTotalUsages.forEach(vacationTotalUsage -> {
            VacationApplyItem vacationApplyMemberItem = new VacationApplyItem.VacationApplyItemBuilder(vacationTotalUsage).build();
            result.add(vacationApplyMemberItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 휴가신청 내역 가져오기
     *
     * @param vacationTotalUsageId 휴가신청내역 시퀀스
     * @return 휴가신청내역 단수
     */
    public VacationTotalUsage getVacationApprovingData(long vacationTotalUsageId) {
        return vacationTotalUsageRepository.findById(vacationTotalUsageId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 휴가 신청 내역을 갖고와 승인 or 반려 처리하고 연차 수량 변경하기
     *
     * @param vacationTotalUsage 휴가 신청 내역(단수)
     * @param approvalStatus     승인, 대기, 반려
     */
    public void putVacationApprovalAdmin(VacationTotalUsage vacationTotalUsage, ApprovalStatus approvalStatus) {
        // 대기 상태가 디폴트, 동일한 승인 상태로 변경할수 없습니다 2중 보안 처리
        if (approvalStatus.equals(vacationTotalUsage.getApprovalStatus())) throw new CImpossibleEqualApprovalStatus();

        VacationCount vacationCount = vacationCountRepository.findById(vacationTotalUsage.getMember().getId()).orElseThrow(CMissingDataException::new);

        // 중복승인 에러, 총 연차 수 < (신청한 개수 + 사용한 개수) 일 경우, exception : ' 직원의 연차 수량이 부족합니다.'
        // 반려 상태로 저장 그게 아니면 승인으로 저장
        if (vacationCount.getCountTotal() < (vacationCount.getCountUse() + vacationTotalUsage.getIncreaseOrDecreaseValue())) {
            vacationTotalUsage.putApprovalStateAdmin(REFER);
            vacationTotalUsageRepository.save(vacationTotalUsage);
            throw new CImpossibleApprovalVacation();
        } else { // 정상적인 신청이라면 승인상태와 연차 수량 변경
            // 반려할 경우
            if (vacationTotalUsage.getApprovalStatus() == REFER) throw new CImpossibleApprovalVacation();
            vacationTotalUsage.putApprovalStateAdmin(approvalStatus);
            vacationTotalUsageRepository.save(vacationTotalUsage);
        }
        // 휴가가 승인이면 연차 수량 테이블에 실제 반영값 증가 시키기 (if 직원이 특별휴가 신청을 했고 승인이라면?)
        if (vacationTotalUsage.getVacationType().equals(VacationType.ANNUAL_LEAVE_PLUS)) {
            vacationCount.addVacationCount(vacationTotalUsage.getFixIncreaseOrDecreaseValue());
        } else // 사용가능 연차 수 -@, 사용한 연차 수 +@
        {
            vacationCount.usingVacationCount(vacationTotalUsage.getFixIncreaseOrDecreaseValue()); // 사용가능 연차수 -@, 사용한 연차 수 +@
        }
        vacationCountRepository.save(vacationCount);
    }

    // Todo 직원 연차별 연차 자동 기입 메서드(미완)
    public void putVacationYearDefault(long memberId, VacationCountOfYearRequest request) {
//        VacationCount vacationCount = vacationCountRepository.findById(memberId).orElseThrow();
//
//        switch (request.getDateJoin()) {
//            case  :
//                this.dateApproval = LocalDateTime.now();
//                this.isApprovalAdmin = true;
////                this.isMinus = true;
//                break;
//            case REFER :
//                this.dateRefer = LocalDateTime.now();
//                break;
//        }
//    }

    }

    // Todo 연차 내역 기간필터(미완)
    public ListResult<MemberAdminUpdate> getListByAdmin(int pageNum, int year, int month, VacationSearchRequest request, ApprovalStatus approvalStatus) {
        Page<VacationTotalUsage> originList;
        PageRequest pageRequest = PageRequest.of(pageNum, 10); // 보통은 10개 보여주니까

//        LocalDateTime dateStart = LocalDateTime.of(year, month, 1, 0, 0, 0);
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.YEAR);

        LocalDateTime dateStart = LocalDateTime.of(
                request.getDateStart().getYear(),
                request.getDateStart().getMonth(),
                request.getDateStart().getDayOfMonth(),
                0,
                0,
                0);
        LocalDateTime dateEnd = LocalDateTime.of(
                request.getDateEnd().getYear(),
                request.getDateEnd().getMonth(),
                request.getDateEnd().getDayOfMonth(),
                23,
                59,
                59);

        if (request.getMemberId() == null) {
            originList = vacationTotalUsageRepository.findAllByApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(approvalStatus, dateStart, dateEnd, pageRequest);
        } else {
            originList = vacationTotalUsageRepository.findAllByMember_IdAndApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(request.getMemberId(), approvalStatus, dateStart, dateEnd, pageRequest);
        }

        // 재가공할 거니까 (임시MemberAdminUpdate 잊지말기)
        List<MemberAdminUpdate> result = new LinkedList<>();
//        originList.getContent().forEach(// 늘 쓰던대로);

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    // Todo 연차 시작일 구하기(미완)
    private LocalDate getCriteriaDateStart(Member member, LocalDate dateJoin) {
        /**
         * 연차 시작일 구하기
         * @param member 직원 시퀀스
         * @param dateJoin 기준일
         * @return
         */
        // 기준일의 년도를 가지고 몇년차인지 구한다.
        // +1의 이유는 예를들면 2020년부터 2022년 근무라고 했을때 2020, 2021, 2022 총 3개를 세야하니까.
        int perYear = dateJoin.getYear() - member.getDateJoin().getYear() + 1;

        // 입사일 기준으로 년차에 해당하는 연차 기준 시작일을 만들어 리턴한다.
        return LocalDate.of(member.getDateJoin().getYear() + (perYear - 1), member.getDateJoin().getMonthValue(), member.getDateJoin().getDayOfMonth());
    }

    // Todo 연차 개수 구하기(미완)
//    public MyVacationTestCountResponse getMyCount(Member member, LocalDate dateCriteria) {
//        // 연차시작일 <= 조회 기준일 and 연차종료일 >= 조회기준일 and 해당회원 조건에 해당하는 데이터를 가져온다.
//        Optional<VacationTestCount> vacationTestCount = vacationTestCountRepository.findByDateStartLessThanEqualAndDateEndGreaterThanEqualAndMemberId(dateCriteria, dateCriteria, member.getId());
//
//        // 만약에 데이터가 있으면 (등록된 연차 초기화 데이터가 있으면)
//        if (vacationTestCount.isPresent()) {
//            // 데이터에서 기준 시작일 가져오고
//            LocalDate dateStart = vacationTestCount.get().getDateStart();
//
//            // 데이터에서 기준 종료일 가져오고
//            LocalDate dateEnd = vacationTestCount.get().getDateEnd();
//
//            // 위의 기간에 연차 신청/승인 된 수 가져오고
//            long countComplete = vacationRepository.countByMemberAndDateHolidayRequestGreaterThanEqualAndDateHolidayRequestLessThanEqualAndIsComplete(member, dateStart, dateEnd, true);
//
//            // 빌더로 값을 넣고 리턴. (기본빌더)
//            return new MyVacationTestCountResponse.MyVacationTestCountResponseBuilder(vacationTestCount.get(), countComplete).build();
//        } else { // 등록된 연차 초기화 데이터가 없으면
//            // 임의로 연차 시작일을 구하고
//            LocalDate dateStart = getCriteriaDateStart(member, dateCriteria);
//            // 연차 시작일에서 1년을 더하고 1일을 빼고
//            LocalDate dateEnd = dateStart.plusYears(1).minusDays(1);
//
//            // 임의로 구한 기간에 연차 신청/승인된 수를 가져오고
//            long countComplete = vacationRepository.countByMemberAndDateHolidayRequestGreaterThanEqualAndDateHolidayRequestLessThanEqualAndIsComplete(member, dateStart, dateEnd, true);
//
//            // 빌더로 값을 넣고 리턴. 위의 빌더와 다른 것 주의.
//            return new MyVacationTestCountResponse.MyVacationTestCountResponseEmptyBuilder(dateStart, dateEnd, countComplete).build();
//        }
//    }
}
