package com.amh.membermanagement.service;

import com.amh.membermanagement.entity.CompanyNews;
import com.amh.membermanagement.exception.common.CMissingDataException;
import com.amh.membermanagement.model.company_news.CompanyNewsItem;
import com.amh.membermanagement.model.company_news.CompanyNewsRequest;
import com.amh.membermanagement.model.common.ListResult;
import com.amh.membermanagement.repository.CompanyNewsRepository;
import com.amh.membermanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CompanyNewsService {

    private final CompanyNewsRepository companyNewsRepository;

    /**
     * 회사 뉴스 등록하기
     * @param request 회사 뉴스 등록 폼
     */
    public void setCompanyNews(CompanyNewsRequest request) {
        CompanyNews companyNews = new CompanyNews.CompanyNewsBuilder(request).build();
        companyNewsRepository.save(companyNews);
    }

    /**
     * 회사 뉴스 가져오기
     * @param id 회사 내역 시퀀스
     * @return 회사 뉴스 리스트
     */
    public CompanyNewsItem getCompanyNews(long id) {
        CompanyNews companyNews = companyNewsRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new CompanyNewsItem.CompanyNewsItemBuilder(companyNews).build();
    }

    /**
     * 전체 회사 뉴스 가져오기
     * @return 회사 뉴스 리스트
     */
    public ListResult<CompanyNewsItem> getCompanyNewsAll() {
        List<CompanyNews> companyNews = companyNewsRepository.findAll();
        List<CompanyNewsItem> result = new LinkedList<>();

        companyNews.forEach(companyNews1 -> {
            CompanyNewsItem addItem = new CompanyNewsItem.CompanyNewsItemBuilder(companyNews1).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    //Todo 회사 뉴스 내용에 이미지 첨부 기능 필요

}
