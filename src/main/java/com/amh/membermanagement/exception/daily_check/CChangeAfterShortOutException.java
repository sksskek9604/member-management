package com.amh.membermanagement.exception.daily_check;

public class CChangeAfterShortOutException extends RuntimeException {

        public CChangeAfterShortOutException(String msg, Throwable t) {
            super(msg, t); //위에 두개 받아줄 것.
        }

        public CChangeAfterShortOutException(String msg) {
            super(msg);
        }

        // 3번째는 빈바구니
        public CChangeAfterShortOutException() {
            super();
    }
}
