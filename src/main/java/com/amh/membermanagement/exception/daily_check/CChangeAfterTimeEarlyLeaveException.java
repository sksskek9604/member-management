package com.amh.membermanagement.exception.daily_check;

public class CChangeAfterTimeEarlyLeaveException extends RuntimeException {

    public CChangeAfterTimeEarlyLeaveException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CChangeAfterTimeEarlyLeaveException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CChangeAfterTimeEarlyLeaveException() {
        super();
    }
}
