package com.amh.membermanagement.exception.daily_check;

public class CChangeBeforeShortOutException extends RuntimeException {

    public CChangeBeforeShortOutException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CChangeBeforeShortOutException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CChangeBeforeShortOutException() {
        super();
    }
}
