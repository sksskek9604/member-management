package com.amh.membermanagement.exception.daily_check;

public class CChangeEqualWorkStateException extends RuntimeException {

    public CChangeEqualWorkStateException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CChangeEqualWorkStateException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CChangeEqualWorkStateException() {
        super();
    }
}
