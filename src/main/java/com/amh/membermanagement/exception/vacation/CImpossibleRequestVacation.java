package com.amh.membermanagement.exception.vacation;

public class CImpossibleRequestVacation extends RuntimeException {
    public CImpossibleRequestVacation(String msg, Throwable t) {
        super(msg, t);
    }

    public CImpossibleRequestVacation(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CImpossibleRequestVacation() {
        super();
    }
}
