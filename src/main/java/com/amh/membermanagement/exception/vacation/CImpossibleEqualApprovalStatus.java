package com.amh.membermanagement.exception.vacation;

public class CImpossibleEqualApprovalStatus extends RuntimeException {
    public CImpossibleEqualApprovalStatus(String msg, Throwable t) {
        super(msg, t);
    }

    public CImpossibleEqualApprovalStatus(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CImpossibleEqualApprovalStatus() {
        super();
    }
}
