package com.amh.membermanagement.exception.vacation;

public class CImpossibleApprovalVacation extends RuntimeException {

    public CImpossibleApprovalVacation(String msg, Throwable t) {
        super(msg, t);
    }

    public CImpossibleApprovalVacation(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CImpossibleApprovalVacation() {
        super();
    }
}
