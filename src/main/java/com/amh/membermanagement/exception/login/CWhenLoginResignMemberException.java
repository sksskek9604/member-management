package com.amh.membermanagement.exception.login;

public class CWhenLoginResignMemberException extends RuntimeException {

    public CWhenLoginResignMemberException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CWhenLoginResignMemberException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CWhenLoginResignMemberException() {
        super();
    }
}
