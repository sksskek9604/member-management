package com.amh.membermanagement.exception.login;

public class CWhenLoginUserNameException extends RuntimeException {

    public CWhenLoginUserNameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CWhenLoginUserNameException(String msg) {
        super(msg);
    }

    public CWhenLoginUserNameException() {
        super();
    }
}
