package com.amh.membermanagement.exception.login;

public class CWhenLoginPasswordException extends RuntimeException {

    public CWhenLoginPasswordException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CWhenLoginPasswordException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CWhenLoginPasswordException() {
        super();
    }
}
