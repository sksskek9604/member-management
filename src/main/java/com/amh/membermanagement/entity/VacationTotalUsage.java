package com.amh.membermanagement.entity;

import com.amh.membermanagement.enums.vacation.ApprovalStatus;
import com.amh.membermanagement.enums.vacation.VacationType;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import com.amh.membermanagement.model.vacation.VacationAdminRequest;
import com.amh.membermanagement.model.vacation.VacationMemberRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationTotalUsage {

    // null허용으로 이 테이블을 직원이 연차신청 / 관리자가 연차 추가 해주는 공통테이블로 사용한다.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "대상 직원 시퀀스", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ApiModelProperty(value = "휴가 종류", required = true)
    @Enumerated(EnumType.STRING)
    private VacationType vacationType;

    @ApiModelProperty(value = "휴가 시작일", required = true)
    private LocalDate vacationStart;

    @ApiModelProperty(value = "휴가 종료일", required = true)
    private LocalDate vacationEnd;

    @ApiModelProperty(value = "신청 사유", required = true)
    @Column(nullable = false, length = 20)
    private String vacationReason;

    @ApiModelProperty(value = "승인 상태", required = true)
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private ApprovalStatus approvalStatus;

    @ApiModelProperty(value = "휴가 증감", required = true)
    @Column(nullable = false)
    private Boolean isMinus;

    @ApiModelProperty(value = "직원 요청 - 증감 개수", required = true)
    @Column(nullable = false)
    private Float increaseOrDecreaseValue;

    @ApiModelProperty(value = "관리자 승인 - 실제 증감 개수", required = true)
    @Column(nullable = false)
    private Float fixIncreaseOrDecreaseValue;

    @ApiModelProperty(value = "승인 시간")
    private LocalDateTime dateApproval;

    @ApiModelProperty(value = "반려 시간")
    private LocalDateTime dateRefer;

    @ApiModelProperty(value = "등록(대기중) 시간")
    @Column(nullable = false)
    private LocalDateTime dateApproving;

    @ApiModelProperty(value = "수정 시간")
    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    // 관리자 휴가 승인과 반려 처리 메서드
    public void putApprovalStateAdmin(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
        switch (approvalStatus) {
            case APPROVAL : // 승인하면?
                this.dateApproval = LocalDateTime.now();
                this.isMinus = true;
                this.fixIncreaseOrDecreaseValue = increaseOrDecreaseValue;
                break;
            case REFER : // 반려하면?
                this.dateRefer = LocalDateTime.now();
                this.isMinus = true;
                this.fixIncreaseOrDecreaseValue = 0f;
                break;
        }
        if (vacationType == VacationType.ANNUAL_LEAVE_PLUS) {
            this.fixIncreaseOrDecreaseValue = increaseOrDecreaseValue;
        }
    }

    private VacationTotalUsage(VacationTotalUsageBuilder builder) {
        this.member = builder.member;
        this.vacationType = builder.vacationType;
        if (vacationType == VacationType.ANNUAL_LEAVE
                || vacationType == VacationType.MONTHLY_LEAVE
                || vacationType == VacationType.HALF_DAY_LEAVE
                || vacationType == VacationType.SICK_LEAVE
        ) {
            this.isMinus = true;
        } else {this.isMinus = false;}
        this.vacationStart = builder.vacationStart;
        this.vacationEnd = builder.vacationEnd;
        this.vacationReason = builder.vacationReason;
        this.approvalStatus = builder.approvalStatus;
        this.increaseOrDecreaseValue = builder.increaseOrDecreaseValue;
        this.fixIncreaseOrDecreaseValue = builder.fixIncreaseOrDecreaseValue;
        this.dateApproving = builder.dateApproving;
        this.dateUpdate = builder.dateUpdate;
    }
    // 관리자는 휴가신청일을 null로 받을 수 있도록 함, 직원은 휴가신청일을 필수로 넣도록 함.

    private VacationTotalUsage(VacationAdminTotalUsageBuilder builder) {
        this.member = builder.member;
        this.vacationType = builder.vacationType;
        if (vacationType == VacationType.ANNUAL_LEAVE_PLUS) {
            this.isMinus = false;
        }
        this.vacationReason = builder.vacationReason;
        this.approvalStatus = builder.approvalStatus;
        this.increaseOrDecreaseValue = builder.increaseOrDecreaseValue;
        this.fixIncreaseOrDecreaseValue = builder.fixIncreaseOrDecreaseValue;
        this.dateApproving = builder.dateApproving;
        this.dateApproval = builder.dateApproval;
        this.dateUpdate = builder.dateUpdate;
    }
    // 관리자는 휴가신청일을 null로 받을 수 있도록 함, 직원은 휴가신청일을 필수로 넣도록 함.

    public static class VacationTotalUsageBuilder implements CommonModelBuilder<VacationTotalUsage> {

        private final Member member;
        private final VacationType vacationType;
        private final LocalDate vacationStart;
        private final LocalDate vacationEnd;
        private final String vacationReason;
        private final ApprovalStatus approvalStatus;
        private final Float increaseOrDecreaseValue;
        private final Float fixIncreaseOrDecreaseValue;
        private final LocalDateTime dateApproving;
        private final LocalDateTime dateUpdate;

        public VacationTotalUsageBuilder(Member member, VacationMemberRequest request) {
            this.member = member;
            this.vacationType = request.getVacationType();
            this.vacationStart = request.getVacationStart();
            this.vacationEnd = request.getVacationEnd();
            this.vacationReason = request.getVacationReason();
            this.approvalStatus = ApprovalStatus.STAND_BY;
            this.increaseOrDecreaseValue = request.getIncreaseOrDecreaseValue();
            this.fixIncreaseOrDecreaseValue = increaseOrDecreaseValue;
            this.dateApproving = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public VacationTotalUsage build() {
            return new VacationTotalUsage(this);
        }
    }

    // 관리자 연차 등록 빌더
    public static class VacationAdminTotalUsageBuilder implements CommonModelBuilder<VacationTotalUsage> {
        private final Member member;
        private final VacationType vacationType;
        private final String vacationReason;
        private final ApprovalStatus approvalStatus;
        private final Float increaseOrDecreaseValue;
        private final Float fixIncreaseOrDecreaseValue;
        private final LocalDateTime dateApproving;
        private final LocalDateTime dateApproval;
        private final LocalDateTime dateUpdate;

        public VacationAdminTotalUsageBuilder(Member member, VacationAdminRequest request) {
            this.member = member;
            this.vacationType = VacationType.ANNUAL_LEAVE_PLUS;
            this.vacationReason = request.getVacationReason();
            this.approvalStatus = ApprovalStatus.APPROVAL;
            this.increaseOrDecreaseValue =  request.getIncreaseOrDecreaseValue();
            this.fixIncreaseOrDecreaseValue = request.getIncreaseOrDecreaseValue();
            this.dateApproving = LocalDateTime.now();
            this.dateApproval = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }
        @Override
        public VacationTotalUsage build() {
            return new VacationTotalUsage(this);
        }
    }
}
