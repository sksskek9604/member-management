package com.amh.membermanagement.entity;

import com.amh.membermanagement.enums.daily_check.DailyCheckState;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DailyCheck {

    // 맨 첫단계에서 데이터가 들어갈 때에는 '출근시간'만 들어간다.
    // 그렇기 때문에 조퇴/퇴근시간 등은 nullable true의 형태이다.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    @ApiModelProperty(value = " 출근 직원", required = true)
    private Member member;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "출근 상태", required = true)
    private DailyCheckState dailyCheckState;
    @Column(nullable = false)
    @ApiModelProperty(value = "기준일(출근일)", required = true)
    private LocalDate dateBase;
    @Column(nullable = false)
    @ApiModelProperty(value = "출근 시간", required = true)
    private LocalTime dateWorkStart;
    @ApiModelProperty(value = "외출 시간", required = true)
    private LocalTime dateShortOuting;

    @ApiModelProperty(value = "외출 복귀 시간", required = true)
    private LocalTime dateWorkComeBack;

    @ApiModelProperty(value = "조퇴 시간", required = true)
    private LocalTime dateEarlyLeave;

    @ApiModelProperty(value = "퇴근 시간", required = true)
    private LocalTime dateWorkEnd;

    @ApiModelProperty(value = "수정 시간", required = true)
    private LocalDateTime dateUpdate;

    // 근태 상태 변화를 위한 메서드
    public void putStateDailyCheck(DailyCheckState dailyCheckState) {
        this.dailyCheckState = dailyCheckState;

        switch (dailyCheckState) {
            case SHORT_OUTING:
                this.dailyCheckState = DailyCheckState.SHORT_OUTING;
                this.dateShortOuting = LocalTime.now();
                this.dateUpdate = LocalDateTime.now();
                break;
            case COME_BACK:
                this.dailyCheckState = DailyCheckState.COME_BACK;
                this.dateWorkComeBack = LocalTime.now();
                this.dateUpdate = LocalDateTime.now();
                break;
            case TIME_EARLY_LEAVE:
                this.dailyCheckState = DailyCheckState.TIME_EARLY_LEAVE;
                this.dateEarlyLeave = LocalTime.now();
                this.dateUpdate = LocalDateTime.now();
                break;
            case WORK_END:
                this.dailyCheckState = DailyCheckState.WORK_END;
                this.dateWorkEnd = LocalTime.now();
                this.dateUpdate = LocalDateTime.now();
                break;
        }
    }
    //빌더 1 출근 버튼
    private DailyCheck(DailyCheckBuilder builder) {
        this.member = builder.member;
        this.dailyCheckState = builder.dailyCheckState;
        this.dateBase = builder.dateBase;
        this.dateWorkStart = builder.dateWorkStart;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class DailyCheckBuilder implements CommonModelBuilder<DailyCheck> {
        private final Member member;
        private final DailyCheckState dailyCheckState;
        private final LocalDate dateBase;
        private final LocalTime dateWorkStart;
        private final LocalDateTime dateUpdate;


        // 직원 entity만 받으면, 나머지는 기본값을 넣을 수 있다. (출근버튼 누르기)
        public DailyCheckBuilder(Member member) {
            this.member = member;
            this.dailyCheckState = DailyCheckState.WORK_START;
            this.dateBase = LocalDate.now();
            this.dateWorkStart = LocalTime.now();
            this.dateUpdate = LocalDateTime.now();
        }
        @Override
        public DailyCheck build() {
            return new DailyCheck(this);
        }
    }

    // 빌더 2 enum - 상태없음을 세팅하기 위한 빌더
    private DailyCheck(DailyCheckNoneBuilder builder) {
        this.dailyCheckState = builder.dailyCheckState;

    }
    public static class DailyCheckNoneBuilder implements CommonModelBuilder<DailyCheck> {
        private final DailyCheckState dailyCheckState;
        public DailyCheckNoneBuilder() {
            this.dailyCheckState = DailyCheckState.UNKNOWN;
        }
        @Override
        public DailyCheck build() {
            return new DailyCheck(this);
        }
    }
}
