package com.amh.membermanagement.entity;

import com.amh.membermanagement.enums.member.Gender;
import com.amh.membermanagement.enums.member.Position;
import com.amh.membermanagement.enums.member.Team;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import com.amh.membermanagement.model.member.admin.MemberAdminUpdate;
import com.amh.membermanagement.model.member.MemberJoinRequest;
import com.amh.membermanagement.model.member.admin.MemberPasswordInit;
import com.amh.membermanagement.model.member.admin.MemberResignUpdate;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    @ApiModelProperty(value = "관리자 권한", required = true)
    private Boolean isAdmin;
    @Column(nullable = false, length = 15, unique = true)
    @ApiModelProperty(value = "아이디", required = true)
    private String username;
    @Column(nullable = false, length = 15)
    @ApiModelProperty(value = "비밀번호", required = true)
    private String password;
    @Column(nullable = false, length = 20)
    @ApiModelProperty(value = "직원 이름", required = true)
    private String memberName;
    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    @ApiModelProperty(value = "소속팀", required = true)
    private Team team;
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    @ApiModelProperty(value = "직급", required = true)
    private Position position;
    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    @ApiModelProperty(value = "성별", required = true)
    private Gender gender;
    @Column(nullable = false, length = 13)
    @ApiModelProperty(value = "연락처", required = true)
    private String memberPhone;
    @Column(nullable = false, length = 40)
    @ApiModelProperty(value = "집 주소", required = true)
    private String memberAddress;
    @Column(nullable = false)
    @ApiModelProperty(value = "입사일", required = true)
    private LocalDate dateJoin;
    @ApiModelProperty(value = "퇴사일", required = true)
    private LocalDate dateResign; // 퇴사일
    @Column(nullable = false)
    @ApiModelProperty(value = "퇴사여부", required = true)
    private Boolean isWorking;
    @Column(nullable = false)
    @ApiModelProperty(value = "등록일", required = true)
    private LocalDateTime dateCreate;
    @Column(nullable = false)
    @ApiModelProperty(value = "수정일", required = true)
    private LocalDateTime dateUpdate;



    public void putMemberPassword(MemberPasswordInit passwordInit) {
        this.password = passwordInit.getPassword();
        this.dateUpdate = LocalDateTime.now();
    }

    public void putMemberAdmin(MemberAdminUpdate isAdminUpdate) {
        this.isAdmin = isAdminUpdate.getIsAdmin();
        this.dateUpdate = LocalDateTime.now();
    }

    public void putMemberResign(MemberResignUpdate resignUpdate) {
        this.dateResign = resignUpdate.getDateResign();
        this.isWorking = false;
        this.dateUpdate = LocalDateTime.now();
    }
    public void putMemberInfo(MemberJoinRequest request) {
        this.username = request.getUsername();
        this.password = request.getPassword();
        this.memberName = request.getMemberName();
        this.team = request.getTeam();
        this.position = request.getPosition();
        this.gender = request.getGender();
        this.memberPhone = request.getMemberPhone();
        this.memberAddress = request.getMemberAddress();
        this.dateJoin = request.getDateJoin();
        this.dateUpdate = LocalDateTime.now();

    }

    private Member(MemberBuilder builder) {
        this.isAdmin = builder.isAdmin;
        this.username = builder.username;
        this.password = builder.password;
        this.memberName = builder.memberName;
        this.team = builder.team;
        this.position = builder.position;
        this.gender = builder.gender;
        this.memberPhone = builder.memberPhone;
        this.memberAddress = builder.memberAddress;
        this.dateJoin = builder.dateJoin;
        this.isWorking = builder.isWorking;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final Boolean isAdmin;
        private final String username;
        private final String password;
        private final String memberName;
        private final Team team;
        private final Position position;
        private final Gender gender;
        private final String memberPhone;
        private final String memberAddress;
        private final LocalDate dateJoin;
        private final Boolean isWorking;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MemberBuilder(MemberJoinRequest request) {
            this.isAdmin = false;
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.memberName = request.getMemberName();
            this.team = request.getTeam();
            this.position = request.getPosition();
            this.gender = request.getGender();
            this.memberPhone = request.getMemberPhone();
            this.memberAddress = request.getMemberAddress();
            this.dateJoin = request.getDateJoin();
            this.isWorking = true;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        } // dateResing 제외함

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
