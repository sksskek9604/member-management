package com.amh.membermanagement.entity;

import com.amh.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberProfileImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    @ApiModelProperty(value = "직원 시퀀스", required = true)
    private Long memberId;

    @Column(nullable = false)
    @ApiModelProperty(value = "이미지 이름", required = true)
    private String imageName;

    @Column(nullable = false)
    @ApiModelProperty(value = "업로드 날짜/시간", required = true)
    private LocalDateTime dateUpload;

    public void putImage(String imageName) {
        this.imageName = imageName;
        this.dateUpload = LocalDateTime.now();
    }

    private MemberProfileImage(MemberProfileImageBuilder builder) {
        this.memberId = builder.memberId;
        this.imageName = builder.imageName;
        this.dateUpload = builder.dateUpload;
    }

    public static class MemberProfileImageBuilder implements CommonModelBuilder<MemberProfileImage> {
        private final Long memberId;
        private final String imageName;
        private final LocalDateTime dateUpload;

        public MemberProfileImageBuilder(Long memberId, String imageName) {
            this.memberId = memberId;
            this.imageName = imageName;
            this.dateUpload = LocalDateTime.now();
        }

        @Override
        public MemberProfileImage build() {
            return new MemberProfileImage(this);
        }
    }

}
