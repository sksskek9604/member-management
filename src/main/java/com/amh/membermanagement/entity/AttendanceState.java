package com.amh.membermanagement.entity;

import com.amh.membermanagement.enums.vacation.ApprovalStatus;
import com.amh.membermanagement.enums.attendance_state.ShiftWorkType;
import com.amh.membermanagement.interfaces.CommonModelBuilder;
import com.amh.membermanagement.model.attendance_state.AttendanceStateInfoRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceState {

    //Todo 근태관리 의 경우는 어플에서 사용하는 경우가 있고, 없는 경우가 있어서 이후 추가 예정

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId",nullable = false)
    private Member member;

    @Column(nullable = false, length = 40)
    private ShiftWorkType shiftWorkType;

    @Column(nullable = false, length = 30)
    private String shiftWorkReason;

    @Column(nullable = false, length = 30)
    private ApprovalStatus approvalStatus;

    @Column(nullable = false)
    private Short workTime;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;
    private LocalDateTime dateApprove; // put할 때 들어갈 것

    private AttendanceState(AttendanceStateBuilder builder) {
        this.member = builder.member;
        this.shiftWorkType = builder.shiftWorkType;
        this.shiftWorkReason = builder.shiftWorkReason;
        this.approvalStatus = builder.approvalStatus;
        this.workTime = builder.workTime;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
        this.dateApprove = builder.dateApprove;
    }

    public static class AttendanceStateBuilder implements CommonModelBuilder<AttendanceState> {

        private final Member member;
        private final ShiftWorkType shiftWorkType;
        private final String shiftWorkReason;
        private final ApprovalStatus approvalStatus;
        private final Short workTime;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;
        private final LocalDateTime dateApprove;

        public AttendanceStateBuilder(Member member, AttendanceStateInfoRequest request) {
            this.member = member;
            this.shiftWorkType = request.getShiftWorkType();
            this.shiftWorkReason = request.getShiftWorkReason();
            this.approvalStatus = request.getApprovalStatus();
            this.workTime = request.getWorkTime();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
            this.dateApprove = null; // 이게 맞나..
        }

        @Override
        public AttendanceState build() {
            return new AttendanceState(this);
        }
    }


}
