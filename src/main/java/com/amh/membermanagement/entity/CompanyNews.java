package com.amh.membermanagement.entity;

import com.amh.membermanagement.interfaces.CommonModelBuilder;
import com.amh.membermanagement.model.company_news.CompanyNewsRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CompanyNews {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    @ApiModelProperty(value = "제목", required = true)
    private String headline;

    @Column(nullable = false, length = 150)
    @ApiModelProperty(value = "내용", required = true)
    private String contents;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private CompanyNews(CompanyNewsBuilder builder) {
        this.headline = builder.headline;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CompanyNewsBuilder implements CommonModelBuilder<CompanyNews> {

        private final String headline;
        private final String contents;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public CompanyNewsBuilder (CompanyNewsRequest request){
            this.headline = request.getHeadline();
            this.contents = request.getContents();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CompanyNews build() {
            return new CompanyNews(this);
        }
    }
}
