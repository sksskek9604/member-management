package com.amh.membermanagement.entity;

import com.amh.membermanagement.interfaces.CommonModelBuilder;
import com.amh.membermanagement.model.vacation.VacationCountOfYearRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationCount {

    /*
    직원 시퀀스를 VacationCount의 시퀀스로 쓸 예정.
    이유는 회원등록시, 휴무개수의 초기 데이터도 같이 생성할 것이기 때문.
    controller에서 회원등록 후, 휴무개수도 등록,
    직원당, 휴무 데이터도 하나 = 회원시퀀스를 이 테이블의 시퀀스로 하는 것.

    값자동이 아닌, 수기로 등록.
    이 필드는 pk이기 때문에, joinColumn(fk)쓰는 방식으로는 엔티티를 끌어올 수 없음
    long으로 수기로 넣어주어야 함. */

    @ApiModelProperty(value = "시퀀스")
    @Id
    private Long memberId;

    @ApiModelProperty(value = "총 연차 개수")
    @Column(nullable = false)
    private Float countTotal; // 반차는 0.5개이기 때문에 개수쪽은 실수형으로 설계

    @ApiModelProperty(value = "사용한 연차 개수")
    @Column(nullable = false)
    private Float countUse; // 반차는 0.5개이기 때문에 개수쪽은 실수형으로 설계

    @ApiModelProperty(value = "사용 가능 연차 개수(잔여)")
    @Column(nullable = false)
    private Float countAvailable;

    @ApiModelProperty(value = "연차 개수 유효 시작일")
    @Column(nullable = false)
    private LocalDate dateVacationCountStart;

    @ApiModelProperty(value = "연차 개수 유효 종료일")
    @Column(nullable = false)
    private LocalDate dateVacationCountEnd;

    @ApiModelProperty(value = "등록시간")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @ApiModelProperty(value = "수정시간")
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void usingVacationCount(float minusCount) {
        this.countUse += minusCount;
        this.countAvailable -= minusCount;
    }

    public void addVacationCount(float plusCount) {
        this.countAvailable += plusCount;
        this.countTotal += plusCount;
    }
    public void plusCountAvailable(float plusCount) {
        this.countAvailable += plusCount;
    }
    public void plusCountTotal(float plusCount) {
        this.countTotal += plusCount; // += 라는 연산자는, 앞에 변수에 뒤에 값을 더한다. 란 뜻이다.
    }
    public void plusCountUse(float plusCount) {
        this.countUse += plusCount;
    }
    private VacationCount(VacationCountBuilder builder) {
        this.memberId = builder.memberId;
        this.countTotal = builder.countTotal;
        this.countUse = builder.countUse;
        this.countAvailable = builder.countAvailable;
        this.dateVacationCountStart = builder.dateVacationCountStart;
        this.dateVacationCountEnd = builder.dateVacationCountEnd;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class VacationCountBuilder implements CommonModelBuilder<VacationCount> {
        private final Long memberId;
        private final Float countTotal; // 반차는 0.5개이기 때문에 개수쪽은 실수형으로 설계
        private final Float countUse; // 반차는 0.5개이기 때문에 개수쪽은 실수형으로 설계
        private final Float countAvailable;
        private final LocalDate dateVacationCountStart;
        private final LocalDate dateVacationCountEnd;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public VacationCountBuilder(Long memberId, LocalDate dateJoin) {
            this.memberId = memberId;
            this.countTotal = 0f; // 최초입사시에는 연차가 없음
            this.countUse = 0f;
            this.countAvailable = this.countTotal - this.countUse;
            this.dateVacationCountStart = dateJoin;// 입사일 기준 연차생성 시작
            this.dateVacationCountEnd =  dateJoin.plusYears(1).minusDays(1);; // 입사일 기준 1년이 유효기간.
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }
         //Todo LocalDateTime.now().plusHours(9) 아이오와 서버를 받고 있기 때문에 시차 계산할 것.
        @Override
        public VacationCount build() {
            return new VacationCount(this);
        }
    }

}
